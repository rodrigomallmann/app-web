From python:alpine

COPY . app

WORKDIR /app

RUN pip install flask

CMD ["python", "app.py"]