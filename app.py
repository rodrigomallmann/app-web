import flask

app = flask.Flask(__name__)

@app.route("/")
def ola():
    mensagem = "Bem vindos ao curso de Python"
    conteudos = [
        'Linguagem',
        'Programacao para redes',
        'Automacao'
    ]
    return flask.render_template("index.html", mensagem=mensagem, conteudos=conteudos)

@app.route("/upper", methods=['POST'])
def upper():
    dados = flask.request.get_json()
    texto = dados['message'].upper()

    return { "message": texto }


app.run(host="0.0.0.0", port=80)