import requests, json

#body = {
#    "acao": "multiplicar",
#    "valorA": 2,
#    "valorB": 3
#}

body = {
    "mensagem": "hello world!"
}

body = json.dumps(body)

headers = {'Content-Type': 'application/json'}

r = requests.post("http://localhost/upper", data=body, headers=headers)

j = r.json()

print(j['mensagem'])
